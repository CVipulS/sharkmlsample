#include <iostream>
#include <shark/Data/Download.h>
#include <shark/Algorithms/Trainers/LDA.h>
#include <shark/ObjectiveFunctions/Loss/ZeroOneLoss.h>
using namespace shark;
int main() {
    ClassificationDataset traindata;
    downloadCsvData(traindata,
                "www.shark-ml.org/data/quickstart-train.csv",
                LAST_COLUMN,
                ' ');
    LinearClassifier<> classifier;
    LDA lda;
    lda.train(classifier, traindata);
    ClassificationDataset testdata;
    downloadCsvData(testdata,
                "www.shark-ml.org/data/quickstart-test.csv",
                LAST_COLUMN,
                ' ');
    ZeroOneLoss<> loss;
    double error = loss(testdata.labels(), classifier(testdata.inputs()));
    std::cout << "RESULTS: " << std::endl << "======== \n" << std::endl << " error " << error << std::endl;
}
